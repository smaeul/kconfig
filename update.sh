#!/bin/sh -eu

if test "$#" -ne 2 ; then
	echo usage: "$0" "</path/to/linux>" "</path/to/package>"
	exit 1
fi

srcdir=$1
pkgdir=$2

overrides=$(tempfile)
trap 'rm -f "$overrides"' EXIT

for config in "$pkgdir"/config-* ; do
	arch=${config##*-}

	# Calculate derived parameters
	case "$arch" in
		*64*) bits=64 ;;
		*)    bits=32 ;;
	esac
	case "$arch" in
		*le)    endian=little ;;
		*be)    endian=big    ;;
		m68k)   endian=big    ;;
		ppc*)   endian=big    ;;
		sparc*) endian=big    ;;
		*)      endian=little ;;
	esac

	# Convert to the kernel ARCH
	case "$arch" in
		aarch64)   ARCH=arm64   ;;
		arm*)      ARCH=arm     ;;
		ppc*)      ARCH=powerpc ;;
		sparc*)    ARCH=sparc   ;;
		*86*|pmmx) ARCH=x86     ;;
		*)         ARCH="$arch" ;;
	esac
	export ARCH

	sed -e 's/[[:space:]]*#.*//g' \
	    -e 's/\(CONFIG[[:alnum:]_]\+\)=n/# \1 is not set/' \
	    generic bits/"$bits" endian/"$endian" arch/"$arch" \
	    > "$overrides"

	(cd "$srcdir" &&
	 cp "$config" .config &&
	 make olddefconfig &&
	 cat .config > "$config")
	(cd "$srcdir" &&
	 scripts/kconfig/merge_config.sh "$config" "$overrides" &&
	 cat .config > "$config")
done

(cd "$pkgdir" && abuild checksum)
